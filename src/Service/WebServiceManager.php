<?php

namespace App\Service;

use App\Entity\Dev;
use App\Entity\Task;
use App\Entity\WebService;
use Doctrine\ORM\EntityManagerInterface;

class WebServiceManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var
     */
    private $message;

    public $dev = array();

    /**
     * WebServiceManager constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager){
        $this->entityManager = $entityManager;
    }

    /**
     * @param $name
     * @param $url
     * @return int|mixed
     */
    public function create($name, $url)
    {
        try {
            $webService = new WebService();
            $webService->setName($name);
            $webService->setUrl($url);
            $this->entityManager->persist($webService);
            $this->entityManager->flush();
            $id = $webService->getId()??0;
        } catch (\Exception $e){
            $id = 0;
            $this->message = $e->getMessage();
        }
        return $id;
    }

    public function store($data)
    {
        try {
            foreach ($data as $record){
                $task = new Task();
                $task->setWs($record['wsId']);
                $task->setTaskId($record['taskId']);
                $task->setTime($record['time']);
                $task->setLevel($record['level']);
                $this->entityManager->persist($task);
            }
            $this->entityManager->flush();
            $id = 1;
        } catch (\Exception $e){
            $id = 0;
            $this->message = $e->getMessage();
        }
        return $id;
    }

    public function setDev()
    {
        $devs = $this->entityManager->getRepository(Dev::class)->findAll();
        foreach ($devs as $dev){
            $this->dev[$dev->getId()] = array(
                'time'=>($dev->getLevel()*$dev->getTime())
            );
        }
    }

    public function assign($week=1)
    {
        $i = 0;
        $tasks = $this->entityManager->getRepository(Task::class)->findBy(array('dev'=>null),['level'=>'desc','time'=>'desc']);
        if($tasks) {
            foreach ($tasks as $task)
            {
                $dev = $this->devSearch($task->getLevel(), $task->getTime());
                if(!is_null($dev)){
                    $table = $this->entityManager->find('App\Entity\Dev', $dev);
                    $task->setDev($table);
                }
                $task->setWeekId($week);
                $this->entityManager->persist($task);
                if(is_null($dev)){
                    $i++;
                }
            }
            $this->entityManager->flush();
            if($i>0){
                $this->setDev();
                $this->assign($week+1);
            }
        }
    }

    public function devSearch($level, $time)
    {
        foreach ($this->dev as $key => $dev) {
            if(isset($dev['time']) and $dev['time']>=($level*$time)) {
                $this->dev[$key]['time'] -= ($level*$time);
                return $key;
            }
        }
    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }
}
