<?php

namespace App\Repository;

use App\Entity\WebService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\String\ByteString;

class WebServiceRepository extends ServiceEntityRepository
{
    /**
     * WebServiceRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WebService::class);
    }

    /**
     * @param $name
     * @return object|null
     */
    public function findOne($name)
    {
        $string = new ByteString($name);
        $array = $string->split('\\');
        return $this->findOneBy(['name'=>$array[count($array)-1]]);
    }
}
