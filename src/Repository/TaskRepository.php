<?php

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class TaskRepository extends ServiceEntityRepository
{
    /**
     * TaskRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    public function findWeekDevTask(): array
    {
        $tasks = array();
        foreach ($this->findBy(array(),['weekId'=>'asc','dev'=>'asc']) as $task){
            $tasks[$task->getWeekId()][$task->getDev()->getName()][] = $task;
        }
        return $tasks;
    }
}
