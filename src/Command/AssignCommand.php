<?php

namespace App\Command;

use App\Service\WebServiceManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AssignCommand extends Command
{
    protected static $defaultName = 'add:task-assign';

    private $webServiceManager;

    public function __construct(WebServiceManager $webServiceManager)
    {
        $this->webServiceManager = $webServiceManager;
        parent::__construct();
    }

    protected function configure()
    {
        //
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->webServiceManager->setDev();
        $this->webServiceManager->assign();
        $output->writeln('Assign Success');
        return Command::SUCCESS;
    }
}
