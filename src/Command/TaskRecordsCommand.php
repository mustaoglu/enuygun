<?php

namespace App\Command;

use App\Service\WebServiceManager;
use App\Support\ProviderGuide;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class TaskRecordsCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'add:task-records';

    private $webServiceManager;

    public function __construct(WebServiceManager $webServiceManager)
    {
        $this->webServiceManager = $webServiceManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->addArgument('service', InputArgument::REQUIRED, 'Service Name')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $guide = new ProviderGuide($input->getArgument('service'));
        $adapter = $guide->getClass($this->webServiceManager->getEntityManager());
        $response = $adapter->getData();
        if($response['status']){
            $return = $this->webServiceManager->store($response['data']);
            if($return){
                $output->writeln('Service Name: '.$input->getArgument('service'));
                $output->writeln($response['message']);
                return Command::SUCCESS;
            }
        } else {
            $output->writeln($response['message']);
        }
        return Command::FAILURE;
    }
}
