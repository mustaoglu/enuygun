<?php
namespace App\Support\Adapters;

use App\Entity\WebService;
use App\Support\ProviderBase;
use App\Support\ProviderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpClient\HttpClient;

class Servis2 extends ProviderBase implements ProviderInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager){
        $this->entityManager = $entityManager;
    }
    public function getData()
    {
        try {
            $webService = $this->entityManager->getRepository(WebService::class)->findOne(get_class());
            $client = HttpClient::create();
            $response = $client->request('GET', $webService->getUrl())->toArray();
            foreach($response as $record){
                $id = key($record);
                $data[] = array(
                    'wsId' => $webService,
                    'taskId' => $id,
                    'time' => $record[$id]['estimated_duration'],
                    'level' => $record[$id]['level']
                );
            }
            return $this->response(1, 'Records data successfully.', $data??array());
        } catch (\Exception $e) {
            return $this->response(0, $e->getMessage(), $e);
        }
    }
}
