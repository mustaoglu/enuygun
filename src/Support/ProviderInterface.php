<?php

namespace App\Support;

interface ProviderInterface
{
    /**
     * @return mixed
     */
    public function getData();
}
