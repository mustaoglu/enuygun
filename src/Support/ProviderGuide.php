<?php

namespace App\Support;

class ProviderGuide
{
    private $namespace = null;

    /**
     * ProviderGuide constructor.
     * @param $provider
     */
    public function __construct($provider)
    {
        $namespace = 'App\\Support\\Adapters\\'.ucfirst($provider);
        if (class_exists($namespace)) {
            $this->namespace = $namespace;
        }
    }

    /**
     * @return mixed
     */
    public function getClass($entityManager)
    {
        return new $this->namespace($entityManager);
    }
}
