<?php

namespace App\Support;

class ProviderBase
{
    /**
     * @param bool $status
     * @param string $message
     * @param array $data
     * @return array
     */
    protected function response($status = false, $message = '', $data = array())
    {
        return [
            'status'  => $status,
            'message' => $message,
            'data' => $data
        ];
    }
}
