<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="tasks")
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 */
class Task
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="WebService", inversedBy="tasks")
     * @ORM\JoinColumn(name="ws_id", referencedColumnName="id")
     */
    private $ws;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $taskId;

    /**
     * @ORM\Column(type="integer")
     */
    private $time;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\ManyToOne(targetEntity="Dev", inversedBy="tasks")
     * @ORM\JoinColumn(name="dev_id", referencedColumnName="id")
     */
    private $dev;

    /**
     * @ORM\Column(type="integer")
     */
    private $weekId;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return WebService|null
     */
    public function getWs(): ?WebService
    {
        return $this->ws;
    }

    /**
     * @return mixed
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @return Dev|null
     */
    public function getDev(): ?Dev
    {
        return $this->dev;
    }

    /**
     * @return mixed
     */
    public function getWeekId()
    {
        return $this->weekId;
    }

    /**
     * @param WebService $webService
     * @return $this
     */
    public function setWs(WebService $webService): self
    {
        $this->ws = $webService;
        return $this;
    }

    /**
     * @param $taskId
     */
    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;
    }

    /**
     * @param $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @param $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @param Dev $dev
     * @return $this
     */
    public function setDev(Dev $dev): self
    {
        $this->dev = $dev;
        return $this;
    }

    /**
     * @param $weekId
     */
    public function setWeekId($weekId)
    {
        $this->weekId = $weekId;
    }
}
