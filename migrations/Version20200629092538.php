<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200629092538 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE devs (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, time INT NOT NULL, level INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tasks (id INT AUTO_INCREMENT NOT NULL, ws_id INT DEFAULT NULL, dev_id INT DEFAULT NULL, task_id VARCHAR(255) NOT NULL, time INT NOT NULL, level INT NOT NULL, week_id INT NOT NULL, INDEX IDX_505865976D5AD892 (ws_id), INDEX IDX_50586597A421F7B0 (dev_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE web_services (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, url LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tasks ADD CONSTRAINT FK_505865976D5AD892 FOREIGN KEY (ws_id) REFERENCES web_services (id)');
        $this->addSql('ALTER TABLE tasks ADD CONSTRAINT FK_50586597A421F7B0 FOREIGN KEY (dev_id) REFERENCES devs (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tasks DROP FOREIGN KEY FK_50586597A421F7B0');
        $this->addSql('ALTER TABLE tasks DROP FOREIGN KEY FK_505865976D5AD892');
        $this->addSql('DROP TABLE devs');
        $this->addSql('DROP TABLE tasks');
        $this->addSql('DROP TABLE web_services');
    }
}
